import { gql } from '@apollo/client';

export const GET_USER = gql`
  query User($userId: String!) {
    user(id: $userId) {
      id,
      name,
      address,
      email,
      birthday
    }
  }
`

export const GET_USER_WITH_POST_TITLES = gql`
  query User($userId: String!) {
    user(id: $userId) {
      id,
      name,
      email,
      posts {
        title,
      }
    }
  }
`

export const GET_POST = gql`
  query Post($postId: String!) {
    post(id: $postId) {
      id,
      title,
      content,
      comments {
        name, 
        content,
      }
    }
  }
`
