import axios from 'axios';
import sizeof from 'object-sizeof';

// Data
import { Experiment } from '../data/experimentsData';
import { postIds } from '../data/postData';
import { userIds } from '../data/userData';

// Axios config
axios.defaults.baseURL = process.env.REACT_APP_API_URL;
const domain = process.env.REACT_APP_DOMAIN;

export const useExperiment = () => {
  const getReferenceTime = async () => {
    const r0 = window.performance.now();
    await axios.get(`${domain}/test.json`, { responseType: 'blob'});
    const r1 = window.performance.now();
    return r1 - r0;
  }

  // REST
  const runRESTExperiment = async (
    experiment: Experiment, requestType: string, requestAmount: number,
  ) => {
    const { requests } = experiment.rest;

    // Get experiment data
    const proceedAllRequests = async (iterator: number,) => {
      const series = async () => {
        const results = [];
        
        for (let i = 0; i < requests.length; i++) {
          const url = requests[i].url
            .replace('$userId', userIds[iterator])
            .replace('$postId', postIds[iterator]);
          const result = await axios.get(url);
          results.push(result.data);
        };
        return results.length > 1 ? results : results[0];
      }
      const res = await series();
      return res;
    };

    // REST sequential requests
    if (requestType === '1') {
      const series = async () => {
        const results = [];
        for (let i = 0; i < requestAmount; i++) {
          const p0 = window.performance.now();
          const result = await proceedAllRequests(i);
          const p1 = window.performance.now();
          results.push({ data: result, time: p1 - p0, size: sizeof(result) / 1024});
        };
        return results;
      }
      const data = await series();
      return { data }
    }

    // REST parallel requests
    if (requestType === '2') {
      const requestPromises = [];
      const p0 = window.performance.now();
      for (let i = 0; i < requestAmount; i++) {
        requestPromises.push(proceedAllRequests(i));
      }
      const responses = await Promise.all(requestPromises);
      const p1 = window.performance.now();
      return { data: responses, timeTotal: p1 - p0, size: sizeof(responses) / 1024};
    }
  }

  // GraphQL
  const runGraphQLExperiment = async (experiment: Experiment, requestType: string, requestAmount: number) => {
    // GraphQL sequential requests
    if (requestType === '1') {
      const series = async () => {
        const results = [];
        
        for (let i = 0; i < requestAmount; i++) {
          const p0 = window.performance.now();
          const result = await axios.post(
            '/graphql',
            { query: experiment.graphQL.request.query, variables: { userId: userIds[i], postId: postIds[i] }}
          );
          const p1 = window.performance.now();
          results.push({
            data: result.data,
            time: p1 - p0,
            size: sizeof(result.data) / 1024,
          });
        }

        return results;
      }
      const data = await series();
      return { data }
    }

    // GraphQL parallel requests
    if (requestType === '2') {
      const p0 = window.performance.now();
      const requestPromises = [];
      
      for (let i = 0; i < requestAmount; i++) {
        requestPromises.push(axios.post(
          '/graphql',
          { query: experiment.graphQL.request.query, variables: { userId: userIds[i], postId: postIds[i] }}
        ));
      }

      const responses = await Promise.all(requestPromises).catch(err => console.log());
      const p1 = window.performance.now();

      const result = responses ? responses.map(item => item.data) : [];

      return { data: result, timeTotal: p1 - p0, size: sizeof(result) / 1024};
    }
  }

  // const addComment = async (postId, name, content) => {
  //   const data = {
  //     userId: userIds[Math.floor(Math.random() * 10)],
  //     postId,
  //     name,
  //     content,
  //   };
  //   console.log('comment', data);
  //   const comment = await axios.post('/api/comments', data);
  //   console.log('---', comment);
  //   return comment.data;
  // }

  // const getFakeData = async () => {
  //   const postIds = [
  //     '6235abea-7fe4-4712-93be-43796f8e97b6',
  //     '6b280d09-91f7-4e13-927e-a39dc9622b97',
  //     '82a0d57e-0515-4be5-9633-12288a5c84dd',
  //     '8746f285-d824-4b3d-8bb0-5b846f303beb',
  //     '8b265bf1-f203-470b-b5db-40f42276ff6b',
  //   ]
  //   let i = 1;
  //   for (const id of postIds) {
  //     const comments = await axios.get(`https://jsonplaceholder.typicode.com/comments?postId=${i}`);
  //     for (const res of comments.data) {
  //       const comment = await addComment(id, res.name, res.body);
  //       console.log(comment.data);
  //     }
  //     i++;
  //   }
  // }

  return [runRESTExperiment, runGraphQLExperiment, getReferenceTime];
};
