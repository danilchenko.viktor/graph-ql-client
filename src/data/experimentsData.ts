export type RestExperiment = {
  requests: { url: string; method: string }[];
};

export type GraphQLExperiment = {
  request: { query: string },
};

export type Experiment = {
  id: number,
  name: string,
  desc: string,
  rest: RestExperiment,
  graphQL: GraphQLExperiment,
  requestAmount: number,
  fixedRequestAmount: boolean,
};

export const experiments: Experiment[] = [
  {
    id: 1,
    name: 'Запит однакових даних',
    desc: 'Отримати данi 10 користувачiв',
    rest: {
      requests: [{ url: '/api/users/$userId', method: 'GET' }],
    },
    graphQL: {
      request: {
        query: `query($userId: String!) {
          user(id: $userId) {
            id,
            name,
            address,
            email,
            birthday
          }
        }`,
      },
    },
    requestAmount: 10,
    fixedRequestAmount: true,
  },
  {
    id: 2,
    name: 'Запит зв\'язаних даних',
    desc: 'Отримати пости користувача з коментарями',
    rest: {
      requests: [
        { url: '/api/posts/$postId', method: 'GET' },
        { url: '/api/comments?postId=$postId', method: 'GET' },
      ],
    },
    graphQL: {
      request: {
        query: `query($postId: String!) {
          post(id: $postId) {
            id,
            title,
            content,
            comments {
              name,
              content,
            }
          }
        }`,
      },
    },
    requestAmount: 10,
    fixedRequestAmount: true,
  },
  {
    id: 3,
    name: 'Запит зв\'язаних часткових даних',
    desc: 'Отримати iм\'я користувача та усi заголовки його постiв',
    rest: {
      requests: [
        { url: '/api/users/$userId', method: 'GET' },
        { url: '/api/posts?userId=$userId', method: 'GET' },
      ],
    },
    graphQL: {
      request: {
        query: `
          query($userId: String!) {
            user(id: $userId) {
              name,
              email,
              posts {
                title,
              }
            }
          }
        `,
      },
    },
    requestAmount: 1,
    fixedRequestAmount: true,
  }
];
