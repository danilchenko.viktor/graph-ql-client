import { SyntheticEvent, useState } from 'react';

import {
  Button,
  Card,
  Col,
  Container,
  Form,
  Row,
} from 'react-bootstrap';

import Loader from '../../components/Loader';

import { Experiment, experiments } from '../../data/experimentsData';
import { useExperiment } from '../../modules/useExperiment';

// Styles
import './Home.css';

const HomePage = () => {
  const [requestType, setRequestType] = useState('1');
  const [experimentType, setExperimentType] = useState<Experiment | null>(null);
  const [finishedExperiments, setFinishedExperiments] = useState([]);
  const [isExperimentRunning, setIsExperimentRunning] = useState(false);
  const [measurementsAmount, setMeasurementsAmount] = useState(10);
  const [reference, setReference] = useState(0);

  const [runExperiment, runGraphQLExperiment, getReferenceTime]: any[] = useExperiment();

  const getDate = () => {
    const d = new Date();
    return d.toLocaleDateString('Ru');
  }

  const getTime = () => {
    const d = new Date();
    return `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
  }

  const handleChangeExperiment = (event: any) => {
    const exp = experiments.find(e => e.id.toString() === event.target.value);
    setExperimentType(exp);
  };

  const handleChangeMeasurementsAmount = (event: any) => {
    setMeasurementsAmount(event.currentTarget.value);
  };

  const updateFinishedExperiments = (experiment) => {
    setFinishedExperiments(prevState => [...prevState, experiment]);
  }

  const getAverageTimeRest = () => {
    const res = finishedExperiments.reduce((acc, curr) => {
      return acc + curr.restAverage;
    }, 0) / finishedExperiments.length;
    return res;
  };

  const getAverageTimeGraph = () => {
    const res = finishedExperiments.reduce((acc, curr) => {
      return acc + curr.graphQlAverage;
    }, 0) / finishedExperiments.length;
    return res;
  };

  const getResumePerformanceText = (restAverage: number, graphQlAverage: number): string => {
    if (graphQlAverage < restAverage) {
      return `GraphQL був швидше нiж REST на ${((restAverage - graphQlAverage) / restAverage * 100).toFixed(2)}%`;
    }
    
    return `REST був швидше нiж GraphQ на ${((graphQlAverage - restAverage) / graphQlAverage * 100).toFixed(2)}%`;
  };

  const getResumeSizeText = (restSizeAverage: number, graphQlSizeAverage: number) => {
    if (graphQlSizeAverage < restSizeAverage) {
      return `Тiло вiдповiдi GraphQL було меньше нiж REST на ${
        ((restSizeAverage - graphQlSizeAverage) / restSizeAverage * 100).toFixed(2)
      }%`;
    }
    
    return `Тiло вiдповiдi REST був було меньше нiж GraphQ на ${
      ((graphQlSizeAverage - restSizeAverage) / graphQlSizeAverage * 100).toFixed(2)
    }%`;
  };

  const handleRunExperiment = async (event: SyntheticEvent) => {
    event.preventDefault();
    setIsExperimentRunning(true);
    setFinishedExperiments([]);

    // reference time
    const referenceTime = await getReferenceTime();
    setReference(referenceTime.toFixed(2));

    for (let i = 0; i < measurementsAmount; i++) {
      const { requestAmount } = experimentType;
    
      const resultRest = await runExperiment(experimentType, requestType, requestAmount) as any;

      const resultGraphQL = await runGraphQLExperiment(experimentType, requestType, requestAmount) as any;      

      let restAverage: number;
      let graphQlAverage: number;
      
      if (requestType === '1') {
        restAverage = resultRest.data.reduce((acc, cur) => acc + cur.time, 0) / resultRest.data.length;
        graphQlAverage = resultGraphQL.data.reduce((acc, cur) => acc + cur.time, 0) / resultGraphQL.data.length;
      }
      
      if (requestType === '2') {
        restAverage = resultRest.timeTotal;
        graphQlAverage = resultGraphQL.timeTotal;
      }
      
      const textResult: string = getResumePerformanceText(restAverage, graphQlAverage);

      const restSizeAverage = resultRest.size
        ? resultRest.size / requestAmount
        : resultRest.data.reduce((acc, cur) => acc + cur.size, 0) / resultRest.data.length;
      const graphQlSizeAverage = resultGraphQL.size
        ? resultGraphQL.size / requestAmount
        : resultGraphQL.data.reduce((acc, cur) => acc + cur.size, 0) / resultGraphQL.data.length;

      const textSizeResult = getResumeSizeText(restSizeAverage, graphQlSizeAverage);

      updateFinishedExperiments({
        experimentType,
        requestType,
        requestAmount,
        rest: resultRest.data,
        restAverage,
        graphql: resultGraphQL.data,
        graphQlAverage,
        textResult,
        timeTotalRest: resultRest.timeTotal,
        timeTotalGraphQl: resultGraphQL.timeTotal,
        restSizeAverage,
        graphQlSizeAverage,
        textSizeResult,
      });
    }

    setIsExperimentRunning(false);
  };

  return (
    <section className="page-layout home-page">
      <Container>
        <h2>Порівняння продуктивності архітектури REST проти мови запитів GraphQL</h2>
      </Container>
      <Container>
        <Row className="mb-4">
          <Col>
            <Card>
              <Card.Body>
                <Card.Title>Керування експериментом</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">Виберіть експеримент, вкажіть кількість запитів та натисніть кнопку "Запустити експеримент"</Card.Subtitle>
                <Form onSubmit={handleRunExperiment}>
                  <Form.Group controlId="experimentId" className="mb-2">
                    <Form.Select
                      name="expId"
                      aria-label="Default select example"
                      onChange={(event: SyntheticEvent<HTMLSelectElement>) => handleChangeExperiment(event)}
                    >
                      <option>Оберiть експеримент</option>
                      {experiments.map((experiment) => (
                        <option
                          key={experiment.id}
                          value={experiment.id}
                          selected={experimentType && experimentType.id === experiment.id}
                        >
                          {experiment.name}
                        </option>
                      ))}
                    </Form.Select>
                  </Form.Group>

                  <p>
                    <span className="text-muted">{'Опис експерименту: '}</span>
                    <i>{experimentType ? experimentType.desc : 'Експеримент не обрано'}</i>
                  </p>

                  <Form.Group controlId="queryType"> 
                    <Form.Label className="radio-label">{'Тип запитів: '}</Form.Label>
                    <Form.Check
                      inline
                      type="radio"
                      name="queryType"
                      id="request-type-1"
                      value="1"
                      label="Послiдовнi запити"
                      checked={requestType === '1'}
                      onChange={() => setRequestType('1')}
                    />
                    <Form.Check
                      inline
                      type="radio"
                      name="queryType"
                      id="request-type-2"
                      value="2"
                      label="Паралельнi запити"
                      checked={requestType === '2'}
                      onChange={() => setRequestType('2')}
                    />
                  </Form.Group>
                  <Form.Group controlId="requestAmount" className="mb-2">
                    <Form.Label>Оберіть кількість вимірювань</Form.Label>
                    <Form.Control
                      type="number"
                      value={measurementsAmount}
                      placeholder="Введiть кiлькiсть вимірювань"
                      onChange={handleChangeMeasurementsAmount}
                      max={100}
                      min={1}
                      name="measurementAmount"
                    />
                  </Form.Group>
                  {experimentType && (
                    <Card className="mb-2">
                      <Card.Body>
                        <Card.Title>REST запити</Card.Title>
                        <ol>
                          {experimentType.rest.requests.map((req) => (
                            <li key={req.url}>{`${req.url}`}</li>
                          ))}
                        </ol>
                      </Card.Body>
                    </Card>
                  )}
                  {experimentType && (
                    <Card className="mb-2">
                      <Card.Body>
                        <Card.Title>GraphQL запит</Card.Title>
                        <ol>
                          <li>{`${experimentType.graphQL.request.query}`}</li>
                        </ol>
                      </Card.Body>
                    </Card>
                  )}
                  {isExperimentRunning ? (
                    <div>
                      <Loader />
                      <p>{`Виконано вимірів: ${finishedExperiments.length} із ${measurementsAmount}`}</p>
                    </div>
                  ) : (
                    <Button variant="primary" type="submit" disabled={!experimentType}>
                      Запустити експеримент
                    </Button>
                  )}
                </Form>
                {/* <Button variant="primary" type="button" onClick={() => getFakeData()}>
                  Data
                </Button> */}
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              <Card.Body>
                <Card.Title>Результати експерименту</Card.Title>
                {!!finishedExperiments.length && (
                  <>
                    <Card.Title>
                      {finishedExperiments[0].experimentType.name}
                      {': '}
                      <em className="text-secondary">{finishedExperiments[0].requestType === '1' ? 'Послiдовнi запити' : 'Паралельнi запити' }</em>
                    </Card.Title>
                    <Card.Subtitle className="mb-1 text-muted">{finishedExperiments[0].experimentType.desc}</Card.Subtitle>
                    <Card.Subtitle className="mb-4 text-muted">{`Кiлькiсть запитiв: ${finishedExperiments[0].requestAmount}`}</Card.Subtitle>
                  </>
                )}
                {finishedExperiments.length > 0 ? (
                  <div>
                    <Card.Subtitle className="mb-2">{`Контрольний вимір: час виконання запиту порожнього JSON файлу ${reference} мс.`}</Card.Subtitle>
                    {finishedExperiments.map((exp, index) => (
                      <Card
                        key={exp.textResult}
                        className="mb-2"
                        style={{ position: "relative", boxShadow: "rgba(0, 0, 0, 0.05) 0px 6px 24px 0px, rgba(0, 0, 0, 0.08) 0px 0px 0px 1px" }}
                      >
                        <Card.Body>
                          <div className="d-flex justify-content-between mb-2">
                            <span className="text-primary">{`Вимірювання №${index + 1}`}</span>
                            <span className="text-primary">{`Дата: ${getDate()} Час: ${getTime()}`}</span>
                          </div>
                          <h5>Результати:</h5>
                          <table className="table table-bordered" style={{ width: '730px' }}>
                            <thead>
                              <tr>
                                <th scope="col" />
                                <th scope="col">REST</th>
                                <th scope="col">GraphQL</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Середня тривалисть виконання запитів</td>
                                <td><strong>{exp.restAverage.toFixed(2)} мс.</strong></td>
                                <td><strong>{exp.graphQlAverage.toFixed(2)} мс.</strong></td>
                              </tr>
                              <tr>
                                <td>Розмiр тiла вiдповiдi</td>
                                <td><strong>{exp.restSizeAverage.toFixed(2)} кб.</strong></td>
                                <td><strong>{exp.graphQlSizeAverage.toFixed(2)} кб.</strong></td>
                              </tr>
                            </tbody>
                          </table>
                          <p className="text-success"><em>{exp.textResult}</em></p>
                          <p className="text-success"><em>{exp.textSizeResult}</em></p>
                          <div className="diagram">
                            <div className="d-rest" style={{ height: `${Math.floor(exp.restAverage) / 3}px` }} />
                            <div className="d-graph" style={{ height: `${Math.floor(exp.graphQlAverage) / 3}px` }} />
                          </div>
                          <div className="diagram-legend">
                            <div className="d-rest-legend">REST</div>
                            <div className="d-graph-legend">GraphQL</div>
                            <div style={{ width: "100%", textAlign: "center" }}>тривалiсть виконання запитiв (нижче - краще)</div>
                          </div>
                        </Card.Body>
                      </Card>
                    ))}
                    <Card>
                      <table className="table table-striped mb-0">
                        <thead>
                          <tr>
                            <th>Запит</th>
                            <th>Кількість вимірювань, од</th>
                            <th>Час виконання, мс</th>
                            <th>Об’єм даних, Кб</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th>файл JSON (контрольний вимір)</th>
                            <td>1</td>
                            <td>{reference}</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <th>REST</th>
                            <td>{finishedExperiments.length}</td>
                            <td>{Math.floor(getAverageTimeRest())}</td>
                            <td>{finishedExperiments[0].restSizeAverage.toFixed(2)}</td>
                          </tr>
                          <tr>
                            <th>GraphQL</th>
                            <td>{finishedExperiments.length}</td>
                            <td>{Math.floor(getAverageTimeGraph())}</td>
                            <td>{finishedExperiments[0].graphQlSizeAverage.toFixed(2)}</td>
                          </tr>
                          <tr>
                            <th>Підсумок</th>
                            <td colSpan={3}>
                              <em className="text-success">{getResumePerformanceText(getAverageTimeRest(), getAverageTimeGraph())}</em><br />
                              <em className="text-success">
                                {getResumeSizeText(finishedExperiments[0].restSizeAverage, finishedExperiments[0].graphQlSizeAverage)}
                              </em>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </Card>
                    </div>
                ) : (
                  <p className="text-secondary">Експеримент не проведено</p>
                )}
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </section>
  )
};

export default HomePage;
