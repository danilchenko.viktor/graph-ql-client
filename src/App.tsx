import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

// Pages
import Home from './pages/Home';

function App() {
  return (
    <div className="root">
      <Home />
    </div>
  );
}

export default App;
